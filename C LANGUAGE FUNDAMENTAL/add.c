#include <stdio.h>
#include <conio.h>
int main(int argc, char const *argv[])
{
  // Adding Two Numbers
  int a;
  int b;
  printf("Enter Number a \n");
  scanf("%d", &a);
  printf("Enter Number b \n");
  scanf("%d", &b);
  int x = a + b;
  printf("Sum of %d and %d is %d", a, b, x);
  return 0;
}
