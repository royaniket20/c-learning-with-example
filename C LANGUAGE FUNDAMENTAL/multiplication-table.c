#include <stdio.h>
#include <conio.h>
/**
 * Print multiplication table entered by the user 1-10 Serise
 */
int main(int argc, char const *argv[])
{
  int a;
  printf("Enter Number for printing Multiplication table  \n");
  scanf("%d", &a);
  printf("Multiplcation Table below : \n");
  for (int i = 1; i <= 10; i++)
  {
    printf("[%d * %d] = %d\n", i, a, a * i);
  }
  return 0;
}
