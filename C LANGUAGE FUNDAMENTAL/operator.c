#include <stdio.h>
#include <conio.h>
int main(int argc, char const *argv[])
{
  int a = 9;
  int b = 2;
  int x = a + b;
  printf("Sum of %d and %d is %d\n", a, b, x);
  printf("Multiplication of %d and %d is %d\n", a, b, a * b);
  printf("Modulus of %d and %d is %d\n", a, b, a % b);
  return 0;
}